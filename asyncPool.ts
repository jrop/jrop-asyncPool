//
// https://maxgreenwald.me/blog/async-pool-promise.all-for-bulk-operations
//
export async function asyncPool<T, U>(
  array: T[],
  poolLimit: number,
  fn: (item: T) => Promise<U>,
): Promise<U[]> {
  const results: Promise<U>[] = [];
  const executing = new Set<Promise<U>>();

  for (const item of array) {
    const prom = Promise.resolve().then(() => fn(item));
    results.push(prom);

    // if the poolLimit is greater/equal to the array length,
    // then we don't need to do anything special: all the
    // promises will be executing concurrently.
    if (poolLimit <= array.length) {
      // track the promise that we just kicked off in our set:
      // it will remove itself when it resolves.
      const cleanup = () => executing.delete(prom);
      executing.add(prom);
      prom.then(cleanup, cleanup);

      // throttle if we are at/past the limit:
      if (executing.size >= poolLimit) {
        await Promise.race(executing);
      }
    }
  }

  return Promise.all(results);
}

export default asyncPool;
