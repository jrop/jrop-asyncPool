import { describe, expect, it } from "bun:test";
import { asyncPool } from "./asyncPool";

describe("asyncPool", () => {
  it("should work", async () => {
    const doubler = (x: number) => Promise.resolve(x * 2);
    const result = await asyncPool([1, 2, 3, 4, 5], 2, doubler);
    expect(result).toEqual([2, 4, 6, 8, 10]);
  });

  it("should throttle", async () => {
    let generation = 0;
    const delay = (ms: number) =>
      new Promise((resolve) => setTimeout(resolve, ms));
    const doubler = async (x: number) => {
      const result = { x: x * 2, generation };
      await delay(100);
      generation++;
      return result;
    };

    const doubled = await asyncPool([1, 2, 3], 2, doubler);
    expect(doubled[0].generation).toEqual(0);
    expect(doubled[1].generation).toEqual(0);
    expect(doubled.map(({ x }) => x)).toEqual([2, 4, 6]);
  });

  it("should work with empty array", async () => {
    const doubler = (x: number) => Promise.resolve(x * 2);
    const result = await asyncPool([], 2, doubler);
    expect(result).toEqual([]);
  });

  it("should run in series if poolLimit is 1", async () => {
    let generation = 0;
    const delay = (ms: number) =>
      new Promise((resolve) => setTimeout(resolve, ms));
    const doubler = async (x: number) => {
      const result = { x: x * 2, generation };
      await delay(100);
      generation++;
      return result;
    };

    const doubled = await asyncPool([1, 2, 3], 1, doubler);
    expect(doubled[0].generation).toEqual(0);
    expect(doubled[1].generation).toEqual(1);
    expect(doubled[2].generation).toEqual(2);
    expect(doubled.map(({ x }) => x)).toEqual([2, 4, 6]);
  });

  it("should throw if any of the promises reject", async () => {
    const doubler = (x: number) => Promise.resolve(x * 2);
    const badDoubler = (_x: number) => Promise.reject(new Error("bad doubler"));
    const result = asyncPool([1, 2, 3, 4, 5], 2, (x) =>
      x === 3 ? badDoubler(x) : doubler(x),
    );
    const promiseValue = await result.then(
      (x) => ({ resolved: x }),
      (x) => ({ rejected: x.message }),
    );
    await expect(promiseValue).toEqual({ rejected: "bad doubler" });
  });
});
